# encoding: utf-8
# -----------------------------------------------------------
# Copyright (C) 2018 Matthias Kuhn
# -----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

from qgis.core import QgsProject  #importa proyecto qgis
from PyQt5 import uic #importa diálogo creado con qtdesigner
import os 
from qgis.core import QgsVectorLayer  

############################################
#copio librerias importadas del script
import processing #importo libreria processing para ejecutar sus algoritmos
from qgis.core import *
import qgis.utils
from PyQt5.QtCore import *
#############################################3

#cargo archivo de diálogo creado con qtdesigner
DialogBase, DialogType = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'cambiosVegetacion_dialogo.ui'))


class GeometryOperationDialog(DialogType, DialogBase): #clase que se importa al archivo principal
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)

        #código necesario para ejecutar la carga de las capas en los combobox
        self.loadLayers()
        self.loadLayers2()	
        self.loadLayers3()	
        self.loadLayers4()	

        #Conecto las funciones con los disparadores
        self.ndviButton.clicked.connect(self.ndvi) 
        self.ndviDifferenceButton.clicked.connect(self.ndviDifference)
        self.saviButton.clicked.connect(self.savi)
        self.saviDifferenceButton.clicked.connect(self.saviDifference)


    def loadLayers(self):
        """
        Carga todas las capas del proyecto y las muestra en el combobox
        """
        for layer in QgsProject.instance().mapLayers().values():
            self.redBandImageAComboBox.addItem(layer.name(), layer)

    def loadLayers2(self):
        for layer in QgsProject.instance().mapLayers().values():
            self.nirBandImageAComboBox.addItem(layer.name(), layer)

    def loadLayers3(self):
        for layer in QgsProject.instance().mapLayers().values():
            self.redBandImageBComboBox.addItem(layer.name(), layer)

    def loadLayers4(self):
        for layer in QgsProject.instance().mapLayers().values():
            self.nirBandImageBComboBox.addItem(layer.name(), layer)





    def ndvi(self): #NDVI (Rouse et al 1974)
        """
        calcula ndvi con las dos capas raster seleccionadas en el combobox
        Almaceno el resultado en una capa de memoria temporal.
        """

        # Cojo la capa seleccionada en el combobox
        redBandImageA = self.redBandImageAComboBox.currentData()
        nirBandImageA = self.nirBandImageAComboBox.currentData()
        redBandImageB = self.redBandImageBComboBox.currentData()
        nirBandImageB = self.nirBandImageBComboBox.currentData()
    
        if nirBandImageA.isValid() is True:
            print("NIR layer image A is valid!")
        else:
            print("NIR layer image A is invalid")

        if redBandImageA.isValid() is True:
            print("RED layer image A is valid!")
        else:
            print("RED layer image A is invalid")

        if nirBandImageB.isValid() is True:
            print("NIR layer image B is valid!")
        else:
            print("NIR layer image B is invalid")

        if redBandImageB.isValid() is True:
            print("RED layer image B is valid!")
        else:
            print("RED layer image B is invalid")



        NDVI_syntax = '((A-B)/(A+B))'

        #llamo a raster calculator para ejecutar la operación ndvo
        processing.algorithmHelp('gdal:rastercalculator')
        output=processing.runAndLoadResults('gdal:rastercalculator', 
            {
            'INPUT_A': nirBandImageA,         #INPUT_A <ParameterRaster>
            'BAND_A': 1,         #BAND_A <ParameterString>
            'INPUT_B': redBandImageA,         #INPUT_B <ParameterRaster>
            'BAND_B': 1,         #BAND_B <ParameterString>
            'FORMULA': NDVI_syntax, #FORMULA <ParameterString>
            'RTYPE': 5,
            'OUTPUT': '/home/darango/Escritorio/TAREA_PLUGIN_CAMBIOS_VEGETACION/resultados/NDVI.tif'           #RTYPE <ParameterSelection>
            })        #OUTPUT <OutputRaster>


        NDVI = QgsRasterLayer(output['OUTPUT'],'NDVI')#genero la capa de salida desde el output de rastercalculator y la llamo NDVI
        if NDVI.isValid() is True:
            print("Output NDVI layer is valid!")
        else:
            print("Output NDVI layer is invalid")


    def ndviDifference(self):
        """
        calcula ndvi difference con las dos capas raster seleccionadas en el combobox
        Almaceno el resultado en una capa de memoria temporal.
        """

        # Cojo la capa seleccionada en el combobox
        redBandImageA = self.redBandImageAComboBox.currentData()
        nirBandImageA = self.nirBandImageAComboBox.currentData()
        redBandImageB = self.redBandImageBComboBox.currentData()
        nirBandImageB = self.nirBandImageBComboBox.currentData()
    
        if nirBandImageA.isValid() is True:
            print("NIR layer image A is valid!")
        else:
            print("NIR layer image A is invalid")

        if redBandImageA.isValid() is True:
            print("RED layer image A is valid!")
        else:
            print("RED layer image A is invalid")

        if nirBandImageB.isValid() is True:
            print("NIR layer image B is valid!")
        else:
            print("NIR layer image B is invalid")

        if redBandImageB.isValid() is True:
            print("RED layer image B is valid!")
        else:
            print("RED layer image B is invalid")


        NDVIDifference_syntax = '((C-D)/(C+D)-(A-B)/(A+B))'

        #llamo a raster calculator para ejecutar la operacion ndvi difference
        processing.algorithmHelp('gdal:rastercalculator')
        output=processing.runAndLoadResults('gdal:rastercalculator', 
            {
            'INPUT_A': nirBandImageA,         #INPUT_A <ParameterRaster>
            'BAND_A': 1,         #BAND_A <ParameterString>
            'INPUT_B': redBandImageA,         #INPUT_B <ParameterRaster>
            'BAND_B': 1,         #BAND_B <ParameterString>
            'INPUT_C': nirBandImageB,         #INPUT_A <ParameterRaster>
            'BAND_C': 1,         #BAND_A <ParameterString>
            'INPUT_D': redBandImageB,         #INPUT_B <ParameterRaster>
            'BAND_D': 1,         #BAND_B <ParameterString>
            'FORMULA': NDVIDifference_syntax, #FORMULA <ParameterString>
            'RTYPE': 5,
            'OUTPUT': '/home/darango/Escritorio/TAREA_PLUGIN_CAMBIOS_VEGETACION/resultados/NDVIDifference.tif'           #RTYPE <ParameterSelection>
            })        #OUTPUT <OutputRaster>


        NDVIDifference = QgsRasterLayer(output['OUTPUT'],'NDVIDifference')#genero la capa de salida desde el output de rastercalculator y la llamo NDVI
        if NDVIDifference.isValid() is True:
            print("Output NDVI Difference layer is valid!")
        else:
            print("Output NDVI Difference layer is invalid")




    def savi(self): #SAVI (Huete 1988)
        """
        calcula savi con las dos capas raster seleccionadas en el combobox
        Almaceno el resultado en una capa de memoria temporal.
        """

        # Cojo la capa seleccionada en el combobox
        redBandImageA = self.redBandImageAComboBox.currentData()
        nirBandImageA = self.nirBandImageAComboBox.currentData()
        redBandImageB = self.redBandImageBComboBox.currentData()
        nirBandImageB = self.nirBandImageBComboBox.currentData()
    
        if nirBandImageA.isValid() is True:
            print("NIR layer image A is valid!")
        else:
            print("NIR layer image A is invalid")

        if redBandImageA.isValid() is True:
            print("RED layer image A is valid!")
        else:
            print("RED layer image A is invalid")

        if nirBandImageB.isValid() is True:
            print("NIR layer image B is valid!")
        else:
            print("NIR layer image B is invalid")

        if redBandImageB.isValid() is True:
            print("RED layer image B is valid!")
        else:
            print("RED layer image B is invalid")



        SAVI_syntax = '(((A-B)*(1.5))/(A+B+0.5))'

        #llamo a raster calculator para ejecutar la operacion savi
        processing.algorithmHelp('gdal:rastercalculator')
        output=processing.runAndLoadResults('gdal:rastercalculator', 
            {
            'INPUT_A': nirBandImageA,         #INPUT_A <ParameterRaster>
            'BAND_A': 1,         #BAND_A <ParameterString>
            'INPUT_B': redBandImageA,         #INPUT_B <ParameterRaster>
            'BAND_B': 1,         #BAND_B <ParameterString>
            'FORMULA': SAVI_syntax, #FORMULA <ParameterString>
            'RTYPE': 5,
            'OUTPUT': '/home/darango/Escritorio/TAREA_PLUGIN_CAMBIOS_VEGETACION/resultados/SAVI.tif'           #RTYPE <ParameterSelection>
            })        #OUTPUT <OutputRaster>


        SAVI = QgsRasterLayer(output['OUTPUT'],'SAVI')#genero la capa de salida desde el output de rastercalculator y la llamo NDVI
        if SAVI.isValid() is True:
            print("Output SAVI layer is valid!")
        else:
            print("Output SAVI layer is invalid")




    def saviDifference(self):
        """
        calcula savi difference con las dos capas raster seleccionadas en el combobox
        Almaceno el resultado en una capa de memoria temporal.
        """

        # Cojo la capa seleccionada en el combobox
        redBandImageA = self.redBandImageAComboBox.currentData()
        nirBandImageA = self.nirBandImageAComboBox.currentData()
        redBandImageB = self.redBandImageBComboBox.currentData()
        nirBandImageB = self.nirBandImageBComboBox.currentData()
    
        if nirBandImageA.isValid() is True:
            print("NIR layer image A is valid!")
        else:
            print("NIR layer image A is invalid")

        if redBandImageA.isValid() is True:
            print("RED layer image A is valid!")
        else:
            print("RED layer image A is invalid")

        if nirBandImageB.isValid() is True:
            print("NIR layer image B is valid!")
        else:
            print("NIR layer image B is invalid")

        if redBandImageB.isValid() is True:
            print("RED layer image B is valid!")
        else:
            print("RED layer image B is invalid")


        SAVIDifference_syntax = '(((C-D)*(1.5))/(C+D+0.5)) - (((A-B)*(1.5))/(A+B+0.5))'

        #llamo a raster calculator para ejecutar la operacion savi difference
        processing.algorithmHelp('gdal:rastercalculator')
        output=processing.runAndLoadResults('gdal:rastercalculator', 
            {
            'INPUT_A': nirBandImageA,         #INPUT_A <ParameterRaster>
            'BAND_A': 1,         #BAND_A <ParameterString>
            'INPUT_B': redBandImageA,         #INPUT_B <ParameterRaster>
            'BAND_B': 1,         #BAND_B <ParameterString>
            'INPUT_C': nirBandImageB,         #INPUT_A <ParameterRaster>
            'BAND_C': 1,         #BAND_A <ParameterString>
            'INPUT_D': redBandImageB,         #INPUT_B <ParameterRaster>
            'BAND_D': 1,         #BAND_B <ParameterString>
            'FORMULA': SAVIDifference_syntax, #FORMULA <ParameterString>
            'RTYPE': 5,
            'OUTPUT': '/home/darango/Escritorio/TAREA_PLUGIN_CAMBIOS_VEGETACION/resultados/SAVIDifference.tif'           #RTYPE <ParameterSelection>
            })        #OUTPUT <OutputRaster>


        SAVIDifference = QgsRasterLayer(output['OUTPUT'],'SAVIDifference')#genero la capa de salida desde el output de rastercalculator y la llamo NDVI
        if SAVIDifference.isValid() is True:
            print("Output SAVI Difference layer is valid!")
        else:
            print("Output SAVI Difference layer is invalid")












         # Creo una capa
    #     uri = "Point?crs={authid}&index=yes".format(authid=layer.crs().authid())
    #     layer_name = "Centroid ({layername})".format(layername=layer.name())
    #     new_layer = QgsVectorLayer(uri, layer_name, "memory")

    #     new_layer.startEditing()

    #     # Añado los atributos
    #     for field in layer.fields():
    #         new_layer.addAttribute(field)

    #     # Proceso todos los features
    #     for feature in layer.getFeatures():
    #         # Here is the important call: create a centroid
    #         geom = feature.geometry().centroid()

    #         feature.setGeometry(geom)
    #         new_layer.addFeature(feature)

    #     # guardo los cambios en una capa de memoria y cargo los resultados en el mapa
    #     new_layer.commitChanges()

    #     QgsProject.instance().addMapLayer(new_layer)


















    # def buffer(self):
    #     """
    #     Genero un buffer sobre la capa seleccionada en el combobox.
    #     Almaceno el buffer creado en una capa de memoria temporal.
    #     """

    #     # Cojo la capa seleccionada en el combobox
    #     layer = self.layerComboBox.currentData() 
    #     # Cojo el valor del tamaño de buffer seleccionado antes
    #     buffer_size = self.bufferSize.value()

    #     # Creo la capa y abro la edición para poder editar cambios en ella
    #     #defino geometría, codigo crs (authid mantiene el crs de la capa original)
    #     uri = "Polygon?crs={authid}&index=yes".format(authid=layer.crs().authid()) #ver uri en api qgis
    #     layer_name = "Buffer ({layername})".format(layername=layer.name())
    #     new_layer = QgsVectorLayer(uri, layer_name, "memory")# parametros: uri (depende del provider),mombre capa, provider)

    #     new_layer.startEditing()

    #     # Añado todos los atributos de la capa 
    #     for field in layer.fields():
    #         new_layer.addAttribute(field)

    #     # Proceso todos los atributos
    #     for feature in layer.getFeatures():
    #         # creo el buffer
    #         geom = feature.geometry().buffer(buffer_size, 5)

    #         feature.setGeometry(geom)
    #         new_layer.addFeature(feature)

    #     # guardo los cambios en la capa y añado la capa al mapa
    #     new_layer.commitChanges()

    #     QgsProject.instance().addMapLayer(new_layer)

    # def centroid(self):
    #     """
    #     Crea un centroide para todos los poligonos de una capa de polígonos seleccionada.
    #     Almaceno el resultado en una capa de memoria temporal.
    #     """

    #     # Cojo la capa seleccionada en el combobox
    #     layer = self.layerComboBox.currentData()

    #     # Creo una capa
    #     uri = "Point?crs={authid}&index=yes".format(authid=layer.crs().authid())
    #     layer_name = "Centroid ({layername})".format(layername=layer.name())
    #     new_layer = QgsVectorLayer(uri, layer_name, "memory")

    #     new_layer.startEditing()

    #     # Añado los atributos
    #     for field in layer.fields():
    #         new_layer.addAttribute(field)

    #     # Proceso todos los features
    #     for feature in layer.getFeatures():
    #         # Here is the important call: create a centroid
    #         geom = feature.geometry().centroid()

    #         feature.setGeometry(geom)
    #         new_layer.addFeature(feature)

    #     # guardo los cambios en una capa de memoria y cargo los resultados en el mapa
    #     new_layer.commitChanges()

    #     QgsProject.instance().addMapLayer(new_layer)
